using System;

namespace Memory
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            using (Game4D game = new Game4D())
            {
                game.Run();
            }
        }
    }
}
