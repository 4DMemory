using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Memory;

namespace Memory
{
    class Playground
    {
        private PlayerIndex m_currentPlayer;
        private Card m_currentCard;

        private CardsGrid m_cardsGrid;

        private Random m_random;

        private static int m_dimension = 10;

        // Summary:
        //      Constructs a new Playground
        public Playground()
        {
            m_currentPlayer = PlayerIndex.One;
            m_currentCard = null;
            m_random = new Random();

            FillField();
        }
        
        // Summary:
        //      Selects a card as the currentPlayer, returns the next player
        // 
        // Parameters:
        //      xpos:
        //          The x coordinate of the card
        //      ypos:
        //          The y coordinate of the card
        //      zpos:
        //          The z coordinate of the card
        // Returns:
        //      The player that gets to select a card next
        public PlayerIndex SelectCard(int xpos, int ypos, int zpos)
        {
            Card pickedCard = m_cardsGrid.getCard(xpos, ypos, zpos);

            // No card selected, this means that the player is selecting their first card
            if (m_currentCard == null)
            {
                m_currentCard = pickedCard;
                return m_currentPlayer;
            }
            // They are selecting their second card, compare with the first card
            else
            {
                if (m_currentCard.Equals(pickedCard))
                {
                    // !! Player selected the correct card
                    return m_currentPlayer;
                }
                else
                {
                    // !! Player selected the wrong card
                    return GetNextPlayer();
                }
            }
        }

        // Summary:
        //      Swaps two planes
        public void SwapPlanes()
        {
            int maxvalue = m_cardsGrid.m_dimension - 1;
            int firstpos = (int)Math.Round(maxvalue * m_random.NextDouble());
            int secondpos = (int)Math.Round(maxvalue * m_random.NextDouble());

            m_cardsGrid.swap(firstpos, secondpos);
        }
        
        // Summary:
        //      Finds the next player and returns it
        //
        // Returns:
        //      The next player
        private PlayerIndex GetNextPlayer()
        {
            int player = (int)m_currentPlayer;
            int nextplayer = (player % 4) + 1;
            m_currentPlayer = (PlayerIndex)nextplayer;
            return m_currentPlayer;
        }

        // Summary:
        //      Initializes all the cards
        private void FillField()
        {
            IList<Card> m_allCards = GenerateCards();
            m_cardsGrid = new CardsGrid(m_dimension);

            for (int x = 0; x < m_dimension; x++)
            {
                for (int y = 0; y < m_dimension; y++)
                {
                    for (int z = 0; z < m_dimension; z++)
                    {
                        Card card = GetRandomCard(m_allCards);
                        m_cardsGrid.setCard(x, y, z, card);
                    }
                }
            }
        }

        // Summary:
        //     Gets a random card from allCards, removes it, and retuns it.
        //
        // Parameters:
        //   allCards:
        //     The list from which a card will be picked
        //
        // Returns:
        //     The selected card
        private Card GetRandomCard(IList<Card> allCards)
        {
            int maxvalue = allCards.Count - 1;
            int pos = (int)Math.Round(maxvalue * m_random.NextDouble());
            Card result = allCards[pos];
            allCards.Remove(result);
            return result;
        }

        // Summary:
        //      Generates a list of Cards, each card is in the list twice
        //
        // Returns:
        //      The generated list
        private IList<Card> GenerateCards()
        {
            int totalCards = (int)Math.Pow(m_dimension, 3);
            IList<Card> result = new List<Card>(totalCards);

            for (int i = 0; i < totalCards / 2; i++)
            {
                Card firtOfPair = new Card(i);
                Card secondOfPair = new Card(i);

                result.Add(firtOfPair);
                result.Add(secondOfPair);
            }

            return result;
        }
    }
}
