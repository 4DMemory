using System;
using System.Collections.Generic;
using System.Text;

namespace Memory
{
    class CardsPlane
    {
        private IList<CardRow> m_cards;

        // Summary:
        //      Initializes a new CardsPlane with specified dimension
        //
        //  Parameters:
        //      dimension:
        //          The dimension of the CardsPlane
        public CardsPlane(int dimension)
        {
            m_cards = new System.Collections.Generic.List<CardRow>(dimension);
            for (int i = 0; i < dimension; i++)
            {
                m_cards[i] = new CardRow(dimension);
            }
        }

        // Summary:
        //      Returns the card at the specified position
        //
        // Parameters:
        //      x: 
        //          The x-coordinate of the card to be retreived
        //      y:
        //          The y-coordinate of the card to be retreived
        //
        // Returns:
        //      The card at the specified position
        public Card getCard(int x, int y)
        {
            CardRow cardrow = m_cards[x];
            return cardrow.getCard(y);
        }

        // Summary:
        //      Sets the card at the specified position
        //
        // Parameters:
        //      x: 
        //          The x-coordinate of the card to be set
        //      y:
        //          The y-coordinate of the card to be set
        //      card:
        //          The card to be set
        public void setCard(int x, int y, Card card)
        {
            CardRow cardrow = m_cards[x];
            cardrow.setCard(y, card);
        }
    }
}
