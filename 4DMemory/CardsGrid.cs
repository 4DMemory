using System;
using System.Collections.Generic;
using System.Text;

namespace Memory
{
    class CardsGrid
    {
        private IList<CardsPlane> m_cardsPlane;
        public int m_dimension
        {
            get
            {
                return m_dimension;
            }
            private set
            {
                m_dimension = value;
            }
        }

        // Summary:
        //      Constructs a CardsGrid with the specified dimension
        //
        // Parameters:
        //      dimension:
        //          The dimension of the CardsGrid
        public CardsGrid(int dimension)
        {
            m_dimension = dimension;
            m_cardsPlane = new List<CardsPlane>(dimension);
        }

        // Summary:
        //      Returns the card at the specified position
        //
        // Parameters:
        //      x: 
        //          The x-coordinate of the card to be retreived
        //      y:
        //          The y-coordinate of the card to be retreived
        //      z:
        //          The z-coordinate of the card to be retreived
        //
        // Returns:
        //      The card at the specified position
        public Card getCard(int x, int y, int z)
        {
            CardsPlane cardsplane = m_cardsPlane[z];
            return cardsplane.getCard(x, y);
        }

        // Summary:
        //      Sets the card at the specified position
        //
        // Parameters:
        //      x: 
        //          The x-coordinate of the card to be set
        //      y:
        //          The y-coordinate of the card to be set
        //      z:
        //          The z-coordinate of the card to be set
        //      card:
        //          The card to be set
        public void setCard(int x, int y, int z, Card card)
        {
            CardsPlane cardsplane = m_cardsPlane[z];
            cardsplane.setCard(x, y, card);
        }

        // Summary:
        //      Swaps the specified two planes, if equal nothing is done
        //
        // Parameters:
        //      firstpos:
        //          The position of the first plane
        //      secondpos:
        //          The position of the second plane
        public void swap(int firstpos, int secondpos)
        {
            // Don't swap anything if they are equal
            if (firstpos == secondpos)
                return;

            CardsPlane firstplane = m_cardsPlane[firstpos];
            CardsPlane secondplane = m_cardsPlane[secondpos];

            m_cardsPlane[firstpos] = secondplane;
            m_cardsPlane[secondpos] = firstplane;
        }
    }
}
