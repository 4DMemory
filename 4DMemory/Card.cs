using System;
using System.Collections.Generic;
using System.Text;

namespace Memory
{
    class Card : IComparable
    {
        private int m_id;

        public Card(int id)
        {
            m_id = id;
        }

        #region IComparable Members

        int IComparable.CompareTo(object obj)
        {
            if ( obj.GetType() == typeof(Card))
                throw new System.ArgumentException("typeof(obj) != Card");

            Card that = (Card)obj;

            if (that.m_id < this.m_id)
                return -1;

            if (that.m_id > this.m_id)
                return 1;

            return 0;
        }

        #endregion
    }
}
