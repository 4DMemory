using System;
using System.Collections.Generic;
using System.Text;

namespace Memory
{
    class CardRow
    {
        IList<Card> m_cards;
        public int m_dimension
        {
            get
            {
                return m_dimension;
            }
            private set
            {
                m_dimension = value;
            }
        }

        // Summary:
        //      Constructs a CardRow with the specified dimension
        //
        // Parameters:
        //      dimension:
        //          The dimension of the CardsGrid
        public CardRow(int dimension)
        {
            m_cards = new List<Card>(dimension);
            m_dimension = dimension;
        }

        // Summary:
        //      Returns the card at the specified position
        //
        // Parameters:
        //      x: 
        //          The x-coordinate of the card to be retreived
        //
        // Returns:
        //      The card at the specified position
        public Card getCard(int y)
        {
            Card card = m_cards[y];
            return card;
        }

        // Summary:
        //      Sets the card at the specified position
        //
        // Parameters:
        //      x: 
        //          The x-coordinate of the card to be set
        //      card:
        //          The card to be set
        public void setCard(int y, Card card)
        {
            m_cards[y] = card;
        }
    }
}
